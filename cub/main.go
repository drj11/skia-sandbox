package main

import (
	"fmt"
	"log"
	"math"
	"os"
)

// solve a cubic

// Represents Ax^3 + Bx^2 + Cx + D,
// but without specifying the variable.
type Cubic struct {
	A, B, C, D float64
}

func (c *Cubic) Eval(x float64) float64 {
	return c.A*x*x*x + c.B*x*x + c.C*x + c.D
}

// Following https://www.johndcook.com/blog/2022/04/05/cubic/
func (c *Cubic) Solve() ([]float64, error) {
	if c.A == 0 {
		return nil, fmt.Errorf("Cubic %v has A=0", *c)
	}
	if c.D == 0 {
		return nil, fmt.Errorf("Cubic %v has D=0", *c)
	}

	// Transform to a polynomial that has
	// exactly one solution between 0 and 1.
	// For detailed notes see Cook article above.

	// Make monic (A is 1), by
	// dividing though by A.
	cub := Cubic{1, c.B / c.A, c.C / c.A, c.D / c.A}

	// Solve related polynomial where D is -1 by
	// replacing x with -cbrt(D)x (and dividing through by -D).
	dd := -cub.D
	cbrtD := math.Cbrt(dd)
	// cub.A *= dd  // cancels out in division
	cub.B /= cbrtD
	cub.C /= cbrtD * cbrtD
	cub.D = -1 // cub.D /= dd

	scale := cbrtD

	// f(0) is -1, and f(x) tends to infinity with x
	// f(1) (is 0, making x=1 a root, or) is either:
	// > 0: meaning there is a root in interval [0 1]; or,
	// < 0: meaning replacing x by 1/x will yield
	// a polynomial with a root in interval [0 1].

	y1 := cub.Eval(1)

	if y1 == 0.0 {
		return []float64{scale}, nil
	}

	reciprocate := false
	if y1 < 0 {
		// Replace x with 1/x and so on.
		// The cute b,c = -c,-b transformation
		// is only possible because a is 1 and d is -1.
		reciprocate = true
		cub.B, cub.C = -cub.C, -cub.B
	}

	x := bisect(func(x float64) bool { return cub.Eval(x) >= 0 }, 0, 1)

	if reciprocate {
		x = 1 / x
	}
	x *= scale

	return []float64{x}, nil
}

// Assuming f is monotonic between l and r
// and f(l) is false (changes from false to true at most once
// between l and r), find the smallest x such that f(x) is true
// and l <= x <= r.
func bisect(f func(x float64) bool, l, r float64) float64 {
	for {
		x := (l + r) / 2
		if x == l || x == r {
			return r
		}
		if f(x) {
			r = x
		} else {
			l = x
		}
	}
}

func main() {
	cub := Cubic{0.5, 0, -1.5, 1}
	xs, err := cub.Solve()
	if err != nil {
		log.Print(err)
	}

	badRoot := 0

	for _, x := range xs {
		y := cub.Eval(x)
		if y != 0 {
			badRoot += 1
		}
		fmt.Println(cub, x, y)
	}
	os.Exit(badRoot)
}
