# An exploration of the skia-python library

`Skia` is a cross-platform path-based 2D graphics library that
also exposes some path-based computations. It appears to be
implemented in C++.

`skia-python` is the Python library binding to that library.

Code in files 010cubic.py and 020pixel.py demonstrate how to use
Skia to draw things with vector graphics (010cubic.py) and
create a simple bi-level pixel renderer (020pixel.py).

## Pixels

The pixel renderer demonstrated in 020pixel.py works by
_evaluating_ a _path_ at the centre of each pixel in a grid.
The evaluation is a two-value (true/false) test of whether a
point is inside the path.

I assume (but haven't checked) that the in/out evaluation uses a
ray to infinity and counts how many times the ray crosses the
path.

That ray test is implemented in the
[Skia `pathops` code](https://github.com/google/skia/blob/main/src/pathops/SkDCubicLineIntersection.cpp).

That code is described as working as follows:

Represent the Cubic Bézier as two equations relating the
parameter _t_ to each of _x_ and _y_:

_x_ = "some cubic polynomial in t, with constants a b c d"
_y_ = "some cubic polynomial in t, with constants e f g h"

and the line as
y = i×x + j (when the line is horizontalish; x = i×y + j when
the line is verticalish)

Q: Should the comment on Line 76 read "for vertical lines"?

Q: In src/pathops/SkDConicLineIntersection.cpp the constructor
for LineConicIntersections sets the max for the intersections (a
container presumably) to 4. With a cryptic comment. The same
value is used in the Cubic code. But shouldn't cubics allow the
1 more intersection. (Aside: why isn't this 3 for cubics and 2
for quadratics?)

Looking at the Conic code and the horizontalIntersect method
that takes more arguments, we can see that the return value of
horizontalIntersect is the number of roots (there is a for loop)
that are valid in the roots array that is passed to
horizontalIntersect.
That makes the root parameter an out parameter.

In general the intersect/thingIntersect methods fill in the
fIntersections member and then return the number of
intersections.

N: In
https://github.com/google/skia/blob/main/src/pathops/SkPathOpsQuad.h
there is a comment (line 87) mentioning cubic.
Suggesting that the cubic code may have been done first, and
then conic/quadratic code derived from it.

The horizontal case is fairly simple, because we can find _t_
without needing the _x_s at all.
The y-coordinate of the curve is y = some_cubic(t)
and the horizontal line is y = k (the y-axis intercept).

Solve for t. Plug that t back in to the cubic description to get
the _x_s and _y_s.

## Some test SVG

https://yqnn.github.io/svg-path-editor/#P=M_2_4_C_2_7_5_7_5_4_M_6_4_C_6_5.6_7.4_7_9_7

# END
