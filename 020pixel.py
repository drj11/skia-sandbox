#!/usr/bin/env python

"""
Render a low-resolution pixel version of the logo path,
by evaluating the path at the centre of each pixel.
The evaluation is a binary in/out.
"""

import math

# https://pypi.org/project/skia-python/
import skia


def letterc():
    """c"""
    return skia.Path().addArc((0, -625, 625, 625), startAngle=50, sweepAngle=260)


def logoDraw():
    """Draw a "cubic" logo.
    Drawn starting at (0,0) on a notional baseline that extends
    to the right, at y=0.
    Hence y coordinate are negative to draw above the baseline.
    Drawn on a nominal design grid of 1000×1000.
    """
    path = skia.Path()
    # c
    c = letterc()
    path.addPath(c)
    # a sort of u (semicircle plus rectangle)
    path.addArc((625, -625, 625, 625), startAngle=0, sweepAngle=180)
    path.addRect(625, -625, 1250, -312)
    # a sort of b
    circ = skia.Path.Circle(312, -312, 312)
    b = skia.Op(circ, skia.Path.Rect((125, -625, 625, 625)), skia.kIntersect_PathOp)
    b = skia.Op(b, skia.Path().addRect(125, -1000, 375, 0), skia.kUnion_PathOp)
    b.transform(skia.Matrix.Translate(1250, 0))
    path.addPath(b)
    # i
    i = skia.Path.Circle(125, -875, 125)
    i.addRect(125, -625, 375, 0)
    i.transform(skia.Matrix.Translate(1875, 0))
    path.addPath(i)
    # c
    c = letterc()
    c.transform(skia.Matrix.Translate(2375, 0))
    path.addPath(c)

    return path


path = logoDraw()

# Pixels per em (em = 1000 design units)
ppem = 8
margin = 1

path.transform(skia.Matrix.Scale(8/1000, 8/1000))

def integerPathBounds(path):
    """
    Return (minx, miny, maxx, maxy) bounds
    for the path, where the returned coordinates.
    The integer bounds are in general up to 1-pixel outside
    the tight bounds.
    """
    fracBounds = path.computeTightBounds()
    minx, miny, maxx, maxy = fracBounds
    minx, miny = map(math.floor, [minx, miny])
    maxx, maxy = map(math.ceil, [maxx, maxy])
    return minx, miny, maxx, maxy

(minx, miny, maxx, maxy) = integerPathBounds(path)

# Position (0,0) at top-left
path.transform(skia.Matrix.Translate(-minx, -miny))
# Add (top left) margin
path.transform(skia.Matrix.Translate(margin, margin))
(minx, miny, maxx, maxy) = integerPathBounds(path)
# For sizing, add oottom right margin
imageX = maxx + margin
imageY = maxy + margin

hit = "@"
miss = "·"
# coordinate of within-pixel sample
atX, atY = (0.5, 0.5)

for j in range(imageY):
    for i in range(imageX):
        x = i+atX
        y = j+atY
        if path.contains(x, y):
            print(hit, end='')
        else:
            print(miss, end='')
    print()

info = skia.ImageInfo.MakeN32(imageX, imageY, skia.AlphaType.kOpaque_AlphaType)
bitmap = skia.Bitmap()
bitmap.tryAllocPixels(info)

for j in range(imageY):
    for i in range(imageX):
        x = i+atX
        y = j+atY
        p = skia.IRect(i, j, i+1, j+1)
        if path.contains(x, y):
            bitmap.erase(skia.ColorBLACK,p)
        else:
            bitmap.erase(skia.ColorMAGENTA,p)

image = skia.Image.MakeFromBitmap(bitmap)
image.save("output.png")

def save(surface, filename):
    """
    Save the surface contents into a PNG file.
    """
    image = surface.makeImageSnapshot()
    image.save(filename, skia.kPNG)

