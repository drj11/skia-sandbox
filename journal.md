# Skia journal

## 2022-08-31

following instructions on https://pypi.org/project/skia-python/

    pip install skia-python

`skia.Path` objects are not self-describing. which is a bit
disappointing.

Workflow from vector description to PNG:

- `skia.Path` to describe vector elements
- `skia.Surface` and its `.getCanvas()` method to get a drawable
  `skia.Canvas`
- `skia.Paint` and `canvas.drawPath()` to place marks on canvas
- `surface.makeImageSnapshot()` and the `.save()` method to
  write to PNG.

Rectangles in `addRect()` are given as (minX, minY, maxX, maxY)
(top-left to bottom-right).

Rectangles used for bounding box for `.addArc()`,
are given as (*top-left, x-extent, y-extent) (4 values).

skia.Path.Rect((x,y,w,h)) (takes a single 4-tuple argument)
creates a path that is a rectangle with top-left (x,y) width w
and height h.

# END
