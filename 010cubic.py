#!/usr/bin/env python

# https://pypi.org/project/skia-python/
import skia


def letterc():
    """c"""
    return skia.Path().addArc((0, -625, 625, 625), startAngle=50, sweepAngle=260)


def logoDraw():
    """Draw a "cubic" logo.
    Drawn starting at (0,0) on a notional baseline that extends
    to the right, at y=0.
    Hence y coordinate are negative to draw above the baseline.
    Drawn on a nominal design grid of 1000×1000.
    """
    path = skia.Path()
    # c
    c = letterc()
    path.addPath(c)
    # a sort of u (semicircle plus rectangle)
    path.addArc((625, -625, 625, 625), startAngle=0, sweepAngle=180)
    path.addRect(625, -625, 1250, -312)
    # a sort of b
    circ = skia.Path.Circle(312, -312, 312)
    b = skia.Op(circ, skia.Path.Rect((125, -625, 625, 625)), skia.kIntersect_PathOp)
    b = skia.Op(b, skia.Path().addRect(125, -1000, 375, 0), skia.kUnion_PathOp)
    b.transform(skia.Matrix.Translate(1250, 0))
    path.addPath(b)
    # i
    i = skia.Path.Circle(125, -875, 125)
    i.addRect(125, -625, 375, 0)
    i.transform(skia.Matrix.Translate(1875, 0))
    path.addPath(i)
    # c
    c = letterc()
    c.transform(skia.Matrix.Translate(2375, 0))
    path.addPath(c)

    return path


path = logoDraw()

width, height = 270, 150
surface = skia.Surface(width, height)
canvas = surface.getCanvas()
canvas.clear(skia.ColorMAGENTA)

paint = skia.Paint(AntiAlias=True)

canvas.concat(skia.Matrix.Translate(20, 100))
canvas.concat(skia.Matrix.Scale(80 / 1000, 80 / 1000))
canvas.drawPath(path, paint)


def save(surface, filename):
    """
    Save the surface contents into a PNG file.
    """
    image = surface.makeImageSnapshot()
    image.save(filename, skia.kPNG)


save(surface, "output.png")
